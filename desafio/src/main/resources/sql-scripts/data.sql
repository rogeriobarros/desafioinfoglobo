INSERT INTO role (ID, ROLE_NAME, DESCRIPTION) VALUES (1, 'STANDARD_USER', 'Usuário padrão do sistema');
INSERT INTO role (ID, ROLE_NAME, DESCRIPTION) VALUES (2, 'ADMIN_USER', 'Usuário administrador do sistema');

-- USER
-- non-encrypted password: jwtpass
INSERT INTO user (ID, FIRST_NAME, LAST_NAME, EMAIL, PASSWORD_HASH, USERNAME, ACTIVATED, CREATED_AT, UPDATED_AT) VALUES (1, 'Didi', 'Mocó', 'didi@moco.com', '821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881231a', 'didi.moco', 1, now(),now());
INSERT INTO user (ID, FIRST_NAME, LAST_NAME, EMAIL, PASSWORD_HASH, USERNAME, ACTIVATED, CREATED_AT, UPDATED_AT) VALUES (2, 'Admin', 'Admin', 'admim@email.com', '821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881231a', 'admin.admin', 1, now(),now());


INSERT INTO user_role(USER_ID, ROLE_ID) VALUES(1,1);
INSERT INTO user_role(USER_ID, ROLE_ID) VALUES(2,1);
INSERT INTO user_role(USER_ID, ROLE_ID) VALUES(2,2);
