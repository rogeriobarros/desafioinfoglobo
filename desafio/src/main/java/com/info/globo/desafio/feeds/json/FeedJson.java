package com.info.globo.desafio.feeds.json;

import java.util.ArrayList;
import java.util.List;

public class FeedJson {

	private List<FeedItemJson> feed;

	public List<FeedItemJson> getFeed() {
		return feed;
	}

	public void addToFeed(FeedItemJson item) {
		if (this.feed == null) {
			this.feed = new ArrayList<FeedItemJson>();
			this.feed.add(item);
		} else {
			this.feed.add(item);
		}
	}

}
