package com.info.globo.desafio.parser;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.info.globo.desafio.feeds.Feed;
import com.info.globo.desafio.feeds.FeedItem;
import com.info.globo.desafio.feeds.json.DescriptionJson;
import com.info.globo.desafio.feeds.json.FeedObjectJson;
import com.info.globo.desafio.feeds.json.FeedItemJson;
import com.info.globo.desafio.feeds.json.FeedJson;

public class ReadTest {
	public static void main(String[] args) {
		RSSFeedParser parser = new RSSFeedParser("https://revistaautoesporte.globo.com/rss/ultimas/feed.xml");
		Feed feed = parser.readFeed();
		
		FeedJson feedJson = new FeedJson();
		
		for (FeedItem feedItem : feed.getItems()) {
			FeedObjectJson feedItemJson = new FeedObjectJson();
			feedItemJson.setTitle(feedItem.getTitle());
			feedItemJson.setLink(feedItem.getLink());

			Document doc = HtmlParser.getContent(StringEscapeUtils.unescapeHtml(feedItem.getDescription()));

			Elements tagsP = doc.select("p");
			for (Element tagP : tagsP) {
				if (tagP.hasText() && tagP.text().matches("\\w.*")) {
					DescriptionJson description = new DescriptionJson();
					description.setType("text");
					description.setContent(tagP.text());
					feedItemJson.addToDescriptions(description);
				}
			}

			Elements tagsImages = doc.select("div > img");
			for (Element tagImage : tagsImages) {
				if (tagImage.hasAttr("src")) {
					DescriptionJson description = new DescriptionJson();
					description.setType("image");
					description.setContent(tagImage.attr("src"));
					feedItemJson.addToDescriptions(description);
				}
			}

			Elements tagsLinks = doc.select("div > ul");
			for (Element tagLink : tagsLinks) {
				DescriptionJson description = new DescriptionJson();
				description.setType("link");
				List<String> link = new ArrayList<String>();
				for (Node dataNode : tagLink.childNodes()) {
					if("li".equals(dataNode.nodeName())) {
						link.add(dataNode.childNode(1).attr("href"));
					}
				}
				description.setContent(link);
				feedItemJson.addToDescriptions(description);
			}
			
			feedJson.addToFeed(new FeedItemJson(feedItemJson));
		}
		
		System.out.println(JsonParser.getJson(feedJson));
	}
}
