package com.info.globo.desafio.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HtmlParser {
	
	public static Document getContent(String html) {
		return Jsoup.parse(html);
	}

}
