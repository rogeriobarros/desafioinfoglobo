package com.info.globo.desafio.parser;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParser {
	
	private static Log log = LogFactory.getLog(JsonParser.class);
	
	public static String getJson(Object obj) {
		ObjectMapper mapper = new ObjectMapper();

		String result = "";
		try {
			result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			log.error("Falha ao processar objeto.", e);
		}

		return result;
	}

}
