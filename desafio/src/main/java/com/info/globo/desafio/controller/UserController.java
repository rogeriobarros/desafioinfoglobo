package com.info.globo.desafio.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.info.globo.desafio.exceptions.UserDuplicateException;
import com.info.globo.desafio.exceptions.UserGenericException;
import com.info.globo.desafio.model.User;
import com.info.globo.desafio.repository.UserRepository;

@RestController
@RequestMapping("/api/usuario")
public class UserController {

	@Autowired(required = true)
	UserRepository userRepository;

	@GetMapping("/list")
	@PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	public Page<User> findPaginated(Pageable pageable) {
		return this.userRepository.findAll(pageable);

	}

	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	public ResponseEntity<User> getUserById(@PathVariable(value = "id") Long id) {
		final User user = this.userRepository.findOne(id);
		if (user == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(user);
	}

	@PostMapping("/create")
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public User createUser(@Valid @RequestBody User user) throws UserDuplicateException, UserGenericException {

		User userPersited = this.userRepository.save(user);

		return userPersited;
	}

	@PutMapping("/edit")
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public ResponseEntity<User> updateUser(@Valid @RequestBody User userIncomming)
			throws UserDuplicateException, UserGenericException {
		final User user = this.userRepository.findOne(userIncomming.getId());
		if (user == null) {
			return ResponseEntity.notFound().build();
		}
		user.setFirstName(userIncomming.getFirstName());
		user.setLastName(userIncomming.getLastName());
		user.setEmail(userIncomming.getEmail());
		user.setUsername(userIncomming.getUsername());
		user.setPassword(userIncomming.getPassword());
		user.setActivated(userIncomming.getActivated());
		
		user.setRoles(userIncomming.getRoles());

		User updatedNote = this.userRepository.save(user);

		return ResponseEntity.ok(updatedNote);
	}

	@DeleteMapping("/remove/{id}")
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public ResponseEntity<User> deleteEmpregado(@PathVariable(value = "id") Long noteId) {
		final User empregado = this.userRepository.findOne(noteId);
		if (empregado == null) {
			return ResponseEntity.notFound().build();
		}

		this.userRepository.delete(empregado);
		return ResponseEntity.ok().build();
	}

}
