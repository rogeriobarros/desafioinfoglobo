package com.info.globo.desafio.feeds.handler;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.info.globo.desafio.feeds.Feed;
import com.info.globo.desafio.feeds.FeedItem;
import com.info.globo.desafio.feeds.json.DescriptionJson;
import com.info.globo.desafio.feeds.json.FeedItemJson;
import com.info.globo.desafio.feeds.json.FeedObjectJson;
import com.info.globo.desafio.feeds.json.FeedJson;
import com.info.globo.desafio.parser.HtmlParser;
import com.info.globo.desafio.parser.JsonParser;
import com.info.globo.desafio.parser.RSSFeedParser;

@Component
public class FeedHandler {

	@Value("${feed.url}")
	private String url;

	public String getUrl() {
		return url;
	}

	private Feed getFeed() {
		RSSFeedParser parser = new RSSFeedParser(this.getUrl());
		Feed feed = parser.readFeed();

		return feed;
	}

	private Document getDocument(String htmlContent) {
		Document document = HtmlParser.getContent(StringEscapeUtils.unescapeHtml(htmlContent));

		return document;
	}

	private void extractFromTagP(Document document, FeedObjectJson feedObjectJson) {
		Elements tagsP = document.select("p");
		for (Element tagP : tagsP) {
			if (tagP.hasText() && tagP.text().matches("\\w.*")) {
				DescriptionJson description = new DescriptionJson();
				description.setType("text");
				description.setContent(tagP.text());
				feedObjectJson.addToDescriptions(description);
			}
		}
	}

	private void extractFromTagImage(Document document, FeedObjectJson feedObjectJson) {
		Elements tagsImages = document.select("div > img");
		for (Element tagImage : tagsImages) {
			if (tagImage.hasAttr("src")) {
				DescriptionJson description = new DescriptionJson();
				description.setType("image");
				description.setContent(tagImage.attr("src"));
				feedObjectJson.addToDescriptions(description);
			}
		}
	}

	private void extractFromTagLinks(Document document, FeedObjectJson feedObjectJson) {
		Elements tagsLinks = document.select("div > ul");
		for (Element tagLink : tagsLinks) {
			DescriptionJson description = new DescriptionJson();
			description.setType("link");
			List<String> link = new ArrayList<String>();
			for (Node dataNode : tagLink.childNodes()) {
				if ("li".equals(dataNode.nodeName())) {
					link.add(dataNode.childNode(1).attr("href"));
				}
			}
			description.setContent(link);
			feedObjectJson.addToDescriptions(description);
		}
	}

	private void preprareFeeds(FeedJson feedJson) {
		Feed feed = this.getFeed();
		for (FeedItem feedItem : feed.getItems()) {
			FeedObjectJson feedObjectJson = new FeedObjectJson();
			feedObjectJson.setTitle(feedItem.getTitle());
			feedObjectJson.setLink(feedItem.getLink());

			Document document = this.getDocument(feedItem.getDescription());

			extractFromTagP(document, feedObjectJson);
			extractFromTagImage(document, feedObjectJson);
			extractFromTagLinks(document, feedObjectJson);

			feedJson.addToFeed(new FeedItemJson(feedObjectJson));
		}
	}

	@Bean
	public String handlerFeeds() {
		FeedJson feedJson = new FeedJson();
		preprareFeeds(feedJson);
		return JsonParser.getJson(feedJson);
	}

}
