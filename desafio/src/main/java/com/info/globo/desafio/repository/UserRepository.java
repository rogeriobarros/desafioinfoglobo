package com.info.globo.desafio.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.info.globo.desafio.model.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
	
	User findByUsername(String username);

}
