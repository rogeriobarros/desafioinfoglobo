package com.info.globo.desafio.feeds.json;

public class DescriptionJson {

	private String type;
	private Object content;

	public DescriptionJson(String type, Object content) {
		super();
		this.type = type;
		this.content = content;
	}

	public DescriptionJson() {
		super();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}

}
