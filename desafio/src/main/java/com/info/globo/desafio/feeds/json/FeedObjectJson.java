package com.info.globo.desafio.feeds.json;

import java.util.ArrayList;
import java.util.List;

public class FeedObjectJson {

	private String title;
	private String link;
	private List<DescriptionJson> description;

	public FeedObjectJson(String title, String link, List<DescriptionJson> description) {
		super();
		this.title = title;
		this.link = link;
		this.description = description;
	}

	public FeedObjectJson() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public List<DescriptionJson> getDescription() {
		return description;
	}

	public void addToDescriptions(DescriptionJson description) {
		if(this.description == null) {
			this.description = new ArrayList<DescriptionJson>();
			this.description.add(description);
		}else {
			this.description.add(description);
		}
	}

}
