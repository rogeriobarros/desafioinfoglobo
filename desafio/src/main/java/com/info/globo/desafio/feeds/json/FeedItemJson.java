package com.info.globo.desafio.feeds.json;

public class FeedItemJson {
	
	private FeedObjectJson item;

	public FeedItemJson(FeedObjectJson item) {
		super();
		this.item = item;
	}

	public FeedObjectJson getItem() {
		return item;
	}

	public void setItem(FeedObjectJson item) {
		this.item = item;
	}

}
