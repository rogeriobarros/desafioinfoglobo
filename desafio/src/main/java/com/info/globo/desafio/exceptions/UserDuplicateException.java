package com.info.globo.desafio.exceptions;

public class UserDuplicateException extends Exception {

	private static final long serialVersionUID = -8264579797772123575L;

	public UserDuplicateException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserDuplicateException(String message) {
		super(message);
	}

}
