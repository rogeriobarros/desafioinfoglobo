package com.info.globo.desafio.parser;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.info.globo.desafio.feeds.Feed;
import com.info.globo.desafio.feeds.FeedItem;
import com.info.globo.desafio.feeds.FeedImage;

public class RSSFeedParser {
	
	private static Log log = LogFactory.getLog(RSSFeedParser.class);

	static final String CHANNEL = "channel";
	static final String TITLE = "title";
	static final String LINK = "link";
	static final String DESCRIPTION = "description";
	static final String LANGUAGE = "language";
	static final String COPYRIGHT = "copyright";
	static final String IMAGE = "image";
	static final String URL = "url";

	static final String ITEM = "item";
	static final String CREATOR = "creator";
	static final String GUID = "guid";

	final URL url;

	public RSSFeedParser(String feedUrl) {
		try {
			this.url = new URL(feedUrl);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	public Feed readFeed() {
		Feed feed = null;
		FeedItem feedItem = null;
		FeedImage image = null;

		try {
			boolean isFeedHeader = true;

			String title = "";
			String link = "";
			String description = "";
			String language = "";
			String copyright = "";
			String creator = "";
			String guid = "";

			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			InputStream in = read();
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();
				if (event.isStartElement()) {
					String localPart = event.asStartElement().getName().getLocalPart();
					switch (localPart) {

					case ITEM:
						if (isFeedHeader) {
							isFeedHeader = false;
							feed = new Feed(title, link, description, language, copyright);
							feed.setImage(image);
						}
						event = eventReader.nextEvent();
						break;
					case TITLE:
						title = getCharacterData(event, eventReader);
						break;
					case DESCRIPTION:
						description = getCharacterData(event, eventReader);
						break;
					case LINK:
						link = getCharacterData(event, eventReader);
						break;
					case LANGUAGE:
						language = getCharacterData(event, eventReader);
						break;
					case COPYRIGHT:
						copyright = getCharacterData(event, eventReader);
						break;
					case IMAGE:
						image = getImage(eventReader);
						break;
					case CREATOR:
						creator = getCharacterData(event, eventReader);
						break;
					case GUID:
						guid = getCharacterData(event, eventReader);
						break;

					}
				} else if (event.isEndElement()) {
					if (event.asEndElement().getName().getLocalPart() == (ITEM)) {
						feedItem = new FeedItem();
						feedItem.setCreator(creator);
						feedItem.setDescription(description);
						feedItem.setGuid(guid);
						feedItem.setLink(link);
						feedItem.setTitle(title);
						feed.addToItems(feedItem);
						event = eventReader.nextEvent();
						continue;
					}
				}
			}
		} catch (XMLStreamException e) {
			throw new RuntimeException(e);
		}
		return feed;
	}

	private FeedImage getImage(XMLEventReader eventReader) throws XMLStreamException {
		FeedImage image = new FeedImage();
		
		while (eventReader.hasNext() && !image.isComplete()) {
			XMLEvent event = eventReader.nextEvent();
			if (event.isStartElement()) {
				String localPart = event.asStartElement().getName().getLocalPart();
				switch (localPart) {
				case URL:
					image.setUrl(getCharacterData(event, eventReader));
					break;
				case TITLE:
					image.setTitle(getCharacterData(event, eventReader));
					break;
				case LINK:
					image.setLink(getCharacterData(event, eventReader));
					break;
				}
			}
		}

		return image;
	}

	private String getCharacterData(XMLEvent event, XMLEventReader eventReader) throws XMLStreamException {
		String result = "";
		event = eventReader.nextEvent();
		if (event instanceof Characters) {
			result = event.asCharacters().getData();
		}
		return result.trim();
	}

	private InputStream read() {
		try {
			return url.openStream();
		} catch (IOException e) {
			log.error("Falha ao obter feed.", e);
			throw new RuntimeException(e);
		}
	}

}
