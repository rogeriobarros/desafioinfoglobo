package com.info.globo.desafio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.info.globo.desafio.feeds.handler.FeedHandler;

@RestController
@RequestMapping("/api/feeds")
public class FeedsController {
	
	@Autowired(required = true)
	FeedHandler handler;
	
	@GetMapping("/all")
	@PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	public String getFeeds() {
		return this.handler.handlerFeeds();

	}

}
