package com.info.globo.desafio.model;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "user")
public class User extends AbstractModelAudit implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, columnDefinition = "NUMBER")
	private Long id;

	@NotNull
	@Pattern(regexp = LOGIN_REGEX)
	@Size(min = 3, max = 25)
	@Column(name = "USERNAME", length = 25, unique = true, nullable = false)
	private String username;

	@NotNull
	@Size(min = 60, max = 60)
	@Column(name = "PASSWORD_HASH", length = 60)
	private String password;

	@Size(max = 50)
	@Column(name = "FIRST_NAME", length = 50)
	private String firstName;

	@Size(max = 50)
	@Column(name = "LAST_NAME", length = 50)
	private String lastName;

	@Email
	@Size(max = 100)
	@Column(name = "EMAIL", length = 100, unique = true)
	private String email;

	@NotNull
	@Column(name = "ACTIVATED", nullable = false, length = 1, columnDefinition = "NUMBER")
	private boolean activated = false;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private List<Role> roles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	// Lowercase the login before saving it in database
	public void setUsername(String login) {
		this.username = login.toLowerCase(Locale.ENGLISH);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean getActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		User user = (User) o;

		if (!username.equals(user.username)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return username.hashCode();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [id=");
		builder.append(id);
		builder.append(", username=");
		builder.append(username);
		builder.append(", password=");
		builder.append(password);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", email=");
		builder.append(email);
		builder.append(", activated=");
		builder.append(activated);
		builder.append("]");
		return builder.toString();
	}

}
