package com.info.globo.desafio.exceptions;

public class UserGenericException extends Exception {

	private static final long serialVersionUID = 4532775922537915349L;

	public UserGenericException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserGenericException(String message) {
		super(message);
	}

}
