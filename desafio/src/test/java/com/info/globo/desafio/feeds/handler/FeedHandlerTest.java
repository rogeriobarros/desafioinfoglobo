package com.info.globo.desafio.feeds.handler;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

@RunWith(MockitoJUnitRunner.class)
public class FeedHandlerTest {
	
	
	@Test
	public void testHandlerFeeds()  {
		
		// QUANDO
		FeedHandler classeEmTeste = Mockito.mock(FeedHandler.class);
		when(classeEmTeste.handlerFeeds()).thenCallRealMethod();
		
		Answer<String> answer = new Answer<String>() {
	        public String answer(InvocationOnMock invocation) throws Throwable {
	            return "https://revistaautoesporte.globo.com/rss/ultimas/feed.xml";
	        }
	    };
	    when(classeEmTeste.getUrl()).thenAnswer(answer);
		
		
		String jsonResult = classeEmTeste.handlerFeeds();

		// ENTÃO
		assertTrue(jsonResult != null);

		try {
			JSONObject jsonObject = new JSONObject(jsonResult);
			assertTrue("Esperado 23 elementos, mas encontrados["+jsonObject.getJSONArray("feed").length()+"]", jsonObject.getJSONArray("feed").length() == 23);
		} catch (JSONException e) {
			fail("Falha ao processar");
		}
	}
}
