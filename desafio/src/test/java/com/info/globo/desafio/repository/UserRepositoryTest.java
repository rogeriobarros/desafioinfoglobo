package com.info.globo.desafio.repository;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.info.globo.desafio.model.User;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
	
	@Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private UserRepository userRepository;
    
    @Test
    public void whenFindByUserName_thenReturnUser() {
        // given
        User user = new User();
        user.setUsername("user.name");
        user.setPassword("821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881");
        user.setCreationDate();
        user.setChangeDate();
        
        entityManager.persist(user);
        entityManager.flush();
     
        // when
        User found = userRepository.findByUsername(user.getUsername());
     
        // then
        assertThat(found.getUsername()).isEqualTo(user.getUsername());
    }

}
