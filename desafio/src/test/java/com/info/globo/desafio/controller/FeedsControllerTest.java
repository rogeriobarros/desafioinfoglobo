package com.info.globo.desafio.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.info.globo.desafio.feeds.handler.FeedHandler;

@RunWith(SpringRunner.class)
@WebMvcTest(FeedsController.class)
@AutoConfigureMockMvc(secure = false)
public class FeedsControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private FeedHandler handler;

	@Test
	public void givenEmployees_whenGetEmployees_thenReturnJsonArray() throws Exception {

		given(handler.handlerFeeds()).willReturn(mockResult());

		mvc.perform(get("/api/feeds/all").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.feed", hasSize(21))).andExpect(jsonPath("$.feed[0].item.title", is("As mulheres que estão fazendo história na indústria automobilística")));
	}

	private String mockResult() {
        String json = null;
        
        File inputXmlFile = new File(this.getClass().getResource("/feed.json").getFile());
        try {
        	json = org.apache.commons.io.FileUtils.readFileToString(inputXmlFile, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json;
	}

}
