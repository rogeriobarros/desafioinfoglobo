# desafioinfoglobo

## Desafio Backend Info Globo

Este projeto usa Spring Boot Application com JSON Web Token (JWT) para proterger a API REST.

## Executar os testes
Para executar os testes. Siga os passos abaixo(SO linux):
 - Abra o terminal
 - Vá para a pasta raíz do projeto
 - Execute o comando: ./gradlew test

## Executar a aplicação
Para executar o aplicativo Spring Boot. Siga os passos abaixo(SO linux):
	- Abra o terminal
 	- Vá para a pasta raíz do projeto
 	- Execute o comando: ./gradlew bootRun 

## Recursos disponíveis:
	- User
		>> [GET] http://localhost:8080/api/usuario/list
		>> [GET] http://localhost:8080/api/usuario/{id}
		>> [POST] http://localhost:8080/api/usuario/create
		>> [PUT] http://localhost:8080/api/usuario/edit
		>> [DELETE] http://localhost:8080/api/usuario/remove/{id}

	- Feeds
		>> [GET] http://localhost:8080/api/feeds/all

## Obter token de acesso
Para acessar os recursos disponíveis é necessário um token JWT. Siga os passos abaixo(SO linux):
	- Abra o terminal
	- Execute a aplicação(ver passo "Executar a aplicação")
	- Abra outro terminal
	- Execute o camando: 
		curl testjwtclientid:XY7kmzoNzl100@localhost:8080/oauth/token -d grant_type=password -d username=admin.admin -d password=jwtpass
	- Pegue do JSON gerado o valor da chave "access_token"
	- Use na chamada a qualquer recurso com o header "Authorization". 
		Ex.: Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsidGVzdGp3dHJlc291cmNlaWQiXSwidXNlcl9uYW1lIjoiYWRtaW4uYWRtaW4iLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiZXhwIjoxNTM1MzcwMDUwLCJhdXRob3JpdGllcyI6WyJTVEFOREFSRF9VU0VSIiwiQURNSU5fVVNFUiJdLCJqdGkiOiI2NWNiNmRmYS05OTdlLTQyYjgtYjNmNS00MzFlYzJhYThmMDciLCJjbGllbnRfaWQiOiJ0ZXN0and0Y2xpZW50aWQifQ.P3jYYR3KRNxcOppQ7qoDSCn6vaejUeS2RMSAguJuvZo

## Json criar novo usuário

{
    "username": "info.globo",
    "firstName": "Info",
    "lastName": "Globo",
    "email": "info@globo.com",
    "password": "821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881",
    "activated": false,
    "roles": [
        {
            "id": 1,
            "roleName": "STANDARD_USER",
            "description": "Usuário padrão do sistema"
        }
    ]
}
